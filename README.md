# Polyglotted IO Parent

A simple parent project for all Polyglotted applications.

### Continuous Integration

[![Circle CI](https://circleci.com/gh/polyglotted/pg-parent.svg?style=shield)](https://circleci.com/gh/polyglotted/pg-parent)
